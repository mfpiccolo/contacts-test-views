require 'spec_helper'

describe Contact do 
  context 'validations' do
    it {should validate_presence_of :name}
  end

  context 'accessibility' do 
    it {should allow_mass_assignment_of :name}
    it {should allow_mass_assignment_of :email}
    it {should allow_mass_assignment_of :phone}
  end
end