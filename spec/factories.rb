require 'spec_helper'

FactoryGirl.define do
  factory :contact do
    name 'michael'
    phone '916-555-1212'
    email 'michael@epicodus.com'
  end
end