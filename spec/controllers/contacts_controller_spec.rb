require 'spec_helper'

describe ContactsController do  
  context 'routing' do
    it {should route(:get, '/contacts/new').to :action => :new}
    it {should route(:post, '/contacts').to :action => :create}
    it {should route(:get, '/contacts/1/edit').to :action => :edit, :id => 1}
    it {should route(:put, '/contacts/1').to :action => :update, :id => 1}
    it {should route(:delete, '/contacts/1').to :action => :destroy, :id => 1}
    it {should route(:get, '/contacts').to :action => :index}
  end

  context 'GET new' do
    before {get :new}

    it {should render_template :new}
  end

  context 'POST create' do
    context 'with valid parameters' do
      let(:valid_attributes) {{:name => 'michael'}}
      let(:valid_parameters) {{:contact => valid_attributes}}

      it 'creates a new contact' do
        expect {post :create, valid_parameters}.to change(Contact, :count).by(1)
      end

      before {post :create, valid_parameters}

      it {should redirect_to contacts_path}
      it {should set_the_flash[:notice]}
    end

    context 'with invalid parameters' do
      let(:invalid_attributes) {{:name => ''}}
      let(:invalid_parameters) {{:contact => invalid_attributes}}
      before {post :create, invalid_parameters}

      it {should render_template :new}
    end
  end

  context 'GET edit' do
    let(:contact) {FactoryGirl.create :contact}
    before {get :edit, :id => contact.id}

    it {should render_template :edit}
  end

  context 'PUT update' do
    let(:contact) {FactoryGirl.create :contact}

    context 'with valid parameters' do
      let(:valid_attributes) {{:email => 'michael@epicodus.com'}}
      let(:valid_parameters) {{:id => contact.id, :contact => valid_attributes}}

      before {put :update, valid_parameters}

      it 'updates the contact' do
        Contact.find(contact.id).email.should eq valid_attributes[:email]
      end

      it {should redirect_to contacts_path}
      it {should set_the_flash[:notice]}
    end

    context 'with invalid parameters' do
      let(:invalid_attributes) {{:name => ''}}
      let(:invalid_parameters) {{:id => contact.id, :contact => invalid_attributes}}
      before {put :update, invalid_parameters}

      it {should render_template :edit}
    end
  end

  context 'DELETE destroy' do
    it 'destroys a contact' do
      contact = FactoryGirl.create :contact
      expect {delete :destroy, {:id => contact.id}}.to change(Contact, :count).by(-1)
    end

    let(:contact) {FactoryGirl.create :contact}
    before {delete :destroy, {:id => contact.id}}

    it {should redirect_to contacts_path}
    it {should set_the_flash[:notice]}
  end

  context 'GET index' do
    before {get :index}

    it {should render_template :index}
  end
end