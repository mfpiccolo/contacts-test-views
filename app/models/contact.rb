class Contact < ActiveRecord::Base
  validates :name, presence: true
  attr_accessible :name, :phone, :email
end