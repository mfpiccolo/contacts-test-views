$('.dropdown-menu').on('click', 'li', function(event) {
  var add, checkbox, tag;

  add = $(this).find('a.add');

  if (add.length > 0) {
    tag = prompt(add.attr('data-prompt'), '');
    $(this).parent().prepend('<li><label>' + tag + '</label></li>');
  }

  event.stopPropagation();
});